using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccess.Abstract;
using Entity.Concrete;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace WepApi.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly IUserRepository _userRepository;

        public UsersController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public Task<IEnumerable<User>> Get()
        {
            return this.GetAll();
        }
        [HttpGet("getall")]

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _userRepository.GetAll();
        }

        [HttpGet("{id}")]
        public Task<User> Get(string id)
        {
            return this.GetUserById(id);
        }

        private async Task<User> GetUserById(string id)
        {
            return await _userRepository.GetById(id);
        }
        
        [HttpPost]
        public async Task<string> Post([FromBody] User user)
        {
            await _userRepository.Add(user);
            return "user added";
        }
        
        [HttpPut("{id}")]
        public async Task<string> Put(string id, [FromBody] User user)
        {
            if (string.IsNullOrEmpty(id)) return "Invalid id!";
            return await _userRepository.Update(id, user);
        }
        
        [HttpDelete("{id}")]
        public async Task<string> Delete(string id)
        {
            if (string.IsNullOrEmpty(id)) return "Invalid id!";
            await _userRepository.Remove(id);
            return "user deleted";
        }
    }
}