using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Entity.Concrete
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public int age { get; set; }
        public Post[] posts { get; set; }
    }
}