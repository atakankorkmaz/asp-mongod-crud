namespace Entity.Concrete
{
    public class Post
    {
        public string title { get; set; }
        public string content { get; set; }
        public int review { get; set; }
    }
}