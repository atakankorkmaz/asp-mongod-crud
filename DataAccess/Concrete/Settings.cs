using Microsoft.Extensions.Configuration;

namespace DataAccess.Concrete
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
        public IConfigurationRoot iConfigurationRoot;
    }
}