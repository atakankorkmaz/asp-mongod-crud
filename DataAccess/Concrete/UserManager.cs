using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccess.Abstract;
using Entity.Concrete;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DataAccess.Concrete
{
    public class UserManager : IUserRepository
    {
        private readonly ObjectContext _context = null;

        public UserManager(IOptions<Settings> settings)
        {
            _context = new ObjectContext(settings);
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            return await _context.Users.Find(x => true).ToListAsync();
        }

        public async  Task<User> GetById(string id)
        {
            var user = Builders<User>.Filter.Eq("Id", id);
            return await _context.Users.Find(user).FirstOrDefaultAsync();
        }

        public async Task Add(User user)
        {
            await _context.Users.InsertOneAsync(user);
        }

        public async Task<string> Update(string id, User user)
        {
            await _context.Users.ReplaceOneAsync(usr => usr.Id == id, user);
            return "";
        }

        public async Task<DeleteResult> Remove(string id)
        {
            return await _context.Users.DeleteOneAsync(Builders<User>.Filter.Eq("Id", id));
        }

        public async Task<DeleteResult> RemoveAll()
        {
            return await _context.Users.DeleteManyAsync(new BsonDocument());
        }
    }
}