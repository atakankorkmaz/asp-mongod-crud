using System.Collections.Generic;
using System.Threading.Tasks;
using Entity.Concrete;
using MongoDB.Driver;

namespace DataAccess.Abstract
{
    public interface IUserRepository
    {
        Task<IEnumerable<User>> GetAll();
        Task<User> GetById(string id);
        Task Add(User user);
        Task<string> Update(string id, User user);
        Task<DeleteResult> Remove(string id);
        Task<DeleteResult> RemoveAll();
    }
}